# KATHRA TEST E2E

E2E tests

## Environments variables

* KATHRA_APPMANAGER_URL : App Manager Service URL
* KATHRA_RESOURCEMANAGER_URL : Resource Manager Service URL
* KEYCLOAK_AUTH_URL : Keycloak authentication URL
* KEYCLOAK_CLIENT_ID : Keycloak client ID
* KEYCLOAK_CLIENT_SECRET : Keycloak client secret
* KEYCLOAK_KATHRA_AUTH_URL : Keycloak authentication realm URL
* USER_LOGIN [no required] : Kathra user login
* USER_PASSWORD [no required] : Kathra user password


## Run test e2e

``
mvn test
``