Feature: User

    @user
    Scenario: Add a new user
        When Create a user in keycloak
        Then User can connect in keycloak
        When Exec synchro user/group
        Then User


    @user
    Scenario: Add a new user and join group
        Given Create a user in keycloak
        Given User can connect in keycloak
        When Add user into group in keycloak
        When Exec synchro user/group
        Then User can see group from appmanager