Feature: ApiVersion

    Background: Init
        Given Init session using user 'kathra-user-e2e-test' and password '1426be2d66ef'
        Given Check group '/kathra-projects/kathra/e2e-test' exists
        Given Check component 'component-e2e-test' exists

    @apiversion
    Scenario: Add a first api version 1.0.0

        Given Remove all version of component 'component-e2e-test'

        When I add apiversion '1.0.0' into component 'component-e2e-test' with file 'swagger-example.yaml'

        Then I have a new apiversion '1.0.0' for component 'component-e2e-test' with status 'PENDING'
        Then Wait until the new apiversion is 'READY', max timeout 600 sec
        Then All apiversion's libraries '1.0.0' are 'READY'
        Then ApiFile 'swagger-example.yaml' for component 'component-e2e-test' at version '1.0.0' is available

    @apiversion
    Scenario: Add a second api version 2.0.0

        Given Check apiversion '1.0.0' for 'component-e2e-test' component exists

        When I add apiversion '2.0.0' into component 'component-e2e-test' with file 'swagger-example.yaml'

        Then I have a new apiversion '2.0.0' for component 'component-e2e-test' with status 'PENDING'
        Then Wait until the new apiversion is 'READY', max timeout 600 sec
        Then All apiversion's libraries '2.0.0' are 'READY'
        Then ApiFile 'swagger-example.yaml' for component 'component-e2e-test' at version '1.0.0' is available


    @apiversion
    Scenario: Update second api version 2.0.0

        Given Check apiversion '2.0.0' for 'component-e2e-test' component exists

        When I update apiversion '2.0.0' into component 'component-e2e-test' with file 'swagger-example.yaml'

        Then I have a apiversion '2.0.0' for component 'component-e2e-test' with status 'UPDATING'
        Then Wait until the updating apiversion is 'READY', max timeout 600 sec
        Then All apiversion's libraries '2.0.0' are 'READY'
        Then ApiFile 'swagger-example.yaml' for component 'component-e2e-test' at version '1.0.0' is available