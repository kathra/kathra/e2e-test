Feature: Implementation

    Background: Init
        Given Init session
        Given Check group '/kathra-projects/kathra/e2e-test' exists
        Given Check component 'component-e2e-test' exists
        Given Check apiversion '1.0.0' for 'component-e2e-test' component exists

    @implementation
    Scenario: Add a new implementation JAVA

        When I add 'JAVA' implementation 'implementation-e2e-test' from component 'component-e2e-test'@'2.0.0'

        Then I have a new 'JAVA' implementation 'implementation-e2e-test' for component 'component-e2e-test'@'2.0.0' with status 'PENDING'
        Then Wait until the new 'JAVA' implementation 'implementation-e2e-test' is 'READY', max timeout 600 sec
        Then The new implementation 'implementation-e2e-test' has a new 'READY' at version '1.0.0' implementing the component 'component-e2e-test'@'2.0.0'

