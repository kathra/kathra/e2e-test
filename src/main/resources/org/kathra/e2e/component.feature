Feature: Component

    Background: Init
        Given Init session using user 'kathra-user-e2e-test' and password '1426be2d66ef'
        Given Check group '/kathra-projects/kathra/e2e-test' exists

    @component
    Scenario: Add a new component
        When I add component 'component-e2e-test' into group '/kathra-projects/kathra/e2e-test'
        Then I have a new component 'component-e2e-test' with status 'PENDING' and group '/kathra-projects/kathra/e2e-test'
        Then Wait until the new component is 'READY', max timeout 90 sec
        Then All component's libraries 'component-e2e-test' are 'READY'
        Then All component's repository 'component-e2e-test' are 'READY'