/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */
package org.kathra.e2e.component;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.kathra.core.model.Component;
import org.kathra.core.model.Library;
import org.kathra.core.model.Resource;
import org.kathra.e2e.utils.KathraApiClients;
import org.kathra.utils.ApiException;
import org.junit.Assert;

import java.util.List;
import java.util.Optional;

public class ComponentSteps extends KathraApiClients {

    @Given("^Check component '(.+)' exists$")
    public void checkExist(String componentName) throws ApiException {
        List<Component> components = componentsClientResourceManager.getComponents();
        Optional<Component> component = components.stream().filter(i -> i.getName().equals(componentName) && i.getStatus().toString().equals("READY")).findFirst();
        Assert.assertTrue(component.isPresent());
        createdComponents.put(componentName, component.get());
    }

    @Given("^Delete component '(.+)' if existing$")
    public void delete(String componentName) throws ApiException {
        List<Component> components = componentsClientResourceManager.getComponents();
        components.stream().filter(component -> component.getName().equalsIgnoreCase(componentName)).forEach(component -> {
            try {
                deleteComponent(component);
            } catch (ApiException e) {
                e.printStackTrace();
            }
        });
    }

    private void deleteComponent(Component component) throws ApiException {
        Component componentWithdetails = componentsClientResourceManager.getComponent(component.getId());
        for (Library library : componentWithdetails.getLibraries()) {
            deleteLibrary(library);
        }
        if (componentWithdetails.getApiRepository() != null && componentWithdetails.getApiRepository().getId() != null) {
            repositoryClientResourceManager.deleteSourceRepository(componentWithdetails.getApiRepository().getId());
        }
        componentsClientResourceManager.deleteComponent(component.getId());
    }
    private void deleteLibrary(Library library) throws ApiException {
        Library libWithDetails = librairiesClientResourceManager.getLibrary(library.getId());
        if (libWithDetails.getSourceRepository() != null && libWithDetails.getSourceRepository().getId() != null) {
            repositoryClientResourceManager.deleteSourceRepository(libWithDetails.getSourceRepository().getId());
        }
        if (libWithDetails.getPipeline() != null && libWithDetails.getPipeline().getId() != null) {
            pipelineClientResourceManager.deletePipeline(libWithDetails.getPipeline().getId());
        }
        librairiesClientResourceManager.deleteLibrary(library.getId());
    }

    @When("^I add component '(.+)' into group '(.+)'$")
    public void create(String componentName, String groupPath) throws ApiException {
        String generatedName = getNoExistingName(componentName);
        Component componentToAdd = new Component().name(generatedName).putMetadataItem("groupPath", groupPath);
        componentAdded = componentsClientAppManager.createComponent(componentToAdd);
        createdComponents.put(componentName, componentAdded);
    }

    private String getNoExistingName(String prefixName) throws ApiException {
        String nameGenerated = prefixName;
        List<Component> components = componentsClientResourceManager.getComponents();
        if (!nameAlreadyExist(components, nameGenerated)) {
            return nameGenerated;
        }
        int i=0;
        do {
            nameGenerated = prefixName+"-"+i;
            i++;
        } while(nameAlreadyExist(components, nameGenerated));
        return nameGenerated;
    }

    private boolean nameAlreadyExist(List<Component> components, String name){
        return components.stream().anyMatch(component -> component.getName().equalsIgnoreCase(name));
    }

    @Then("^I have a new component '(.+)' with status '(.+)' and group '(.+)'$")
    public void check(String componentName, String status, String groupPath) throws ApiException {
        Assert.assertNotNull(componentAdded);
        Assert.assertTrue(componentAdded.getName().contains(createdComponents.get(componentName).getName()));
        Assert.assertEquals(status, componentAdded.getStatus().toString());
        Assert.assertEquals(groupPath, componentAdded.getMetadata().get("groupPath"));
    }


    @Then("^Wait until the new component is '(.+)', max timeout (\\d+) sec$")
    public void checkUntilHasStatusOrFailed(String status, int timeout) throws ApiException, InterruptedException {
        Assert.assertNotNull(componentAdded);
        Component componentAddedUpdated;
        long startTime = System.currentTimeMillis();
        do {
            componentAddedUpdated = componentsClientAppManager.getComponentById(componentAdded.getId());
            Assert.assertNotNull(componentAddedUpdated);
            Thread.sleep(1000);
        } while(componentAddedUpdated.getStatus().toString().equalsIgnoreCase(Resource.StatusEnum.PENDING.toString()) && System.currentTimeMillis() - startTime < timeout * 1000);
        Assert.assertEquals(status, componentAddedUpdated.getStatus().toString());
    }

    @When("^All component's libraries '(.+)' are '(.+)'$")
    public void checkComponentLib(String componentName, String status) throws ApiException {
        Component component = componentsClientAppManager.getComponentById(createdComponents.get(componentName).getId());
        List<Library> libs = component.getLibraries();
        for(Library lib:libs) {
            Library libWithDetails = librairiesClientResourceManager.getLibrary(lib.getId());
            Assert.assertEquals(status, libWithDetails.getStatus().toString());
            Assert.assertEquals(status, repositoryClientResourceManager.getSourceRepository(libWithDetails.getSourceRepository().getId()).getStatus().toString());
            Assert.assertEquals(status, pipelineClientResourceManager.getPipeline(libWithDetails.getPipeline().getId()).getStatus().toString());
        }
    }

    @When("^All component's repository '(.+)' are '(.+)'$")
    public void checkComponentApiRepository(String componentName, String status) throws ApiException {
        Component component = componentsClientAppManager.getComponentById(createdComponents.get(componentName).getId());
        Assert.assertEquals(status, repositoryClientResourceManager.getSourceRepository(component.getApiRepository().getId()).getStatus().toString());
    }
}