/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */
package org.kathra.e2e.apiversion;


import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.util.Encoding;
import org.kathra.core.model.*;
import org.kathra.e2e.utils.KathraApiClients;
import org.kathra.utils.ApiException;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Optional;

public class ApiVersionSteps extends KathraApiClients {

    @Given("^Check apiversion '(.+)' for '(.+)' component exists$")
    public void checkExist(String version, String componentName) throws ApiException {
        List<Component> components = componentsClientResourceManager.getComponents();
        Optional<Component> component = components.stream().filter(i -> i.getName().equals(componentName) && i.getStatus().toString().equals("READY")).findFirst();
        Assert.assertTrue(component.isPresent());
        Optional<ApiVersion> apiVersion = component.get().getVersions().stream().filter(i -> {
            try {
                ApiVersion apiVersionDetailed = apiVersionsClientResourceManager.getApiVersion(i.getId());
                return apiVersionDetailed.getVersion().equals(version) && apiVersionDetailed.getStatus().toString().equals("READY");
            } catch (ApiException e) {
                return false;
            }
        }).findFirst();
        Assert.assertTrue(apiVersion.isPresent());
        createdApiVersions.put(version, apiVersion.get());
    }

    @When("^I add apiversion '(.+)' into component '(.+)' with file '(.+)'$")
    public void create(String version, String componentName, String filename) throws Exception {
        File file = new File("./src/main/resources/org/kathra/e2e/"+filename);
        File fileWithVersion = getFileCustomized(file, version, createdComponents.get(componentName).getName());
        apiVersionAdded = apiVersionsClientAppManager.createApiVersion(createdComponents.get(componentName).getId(), fileWithVersion);
        createdApiVersions.put(version, apiVersionAdded);
    }

    @When("^I update apiversion '(.+)' into component '(.+)' with file '(.+)'$")
    public void update(String version, String componentName, String filename) throws Exception {
        File file = new File("./src/main/resources/org/kathra/e2e/"+filename);
        File fileWithVersion = getFileCustomized(file, version, createdComponents.get(componentName).getName());
        String apiVersionToUpdateId = createdApiVersions.get(version).getId();
        apiVersionAdded = apiVersionsClientAppManager.updateApiVersion(apiVersionToUpdateId, fileWithVersion);
        createdApiVersions.put(version, apiVersionAdded);
    }

    @Given("Remove all version of component '(.+)'")
    public void removeAllApiVersionForComponent(String componentName) throws ApiException {
        Optional<Component> componentFound = componentsClientResourceManager.getComponents().stream().filter(component -> component.getName().equals(componentName)).findFirst();
        componentFound.get().getVersions().stream().forEach(apiVersion -> {
            try {
                delete(apiVersionsClientResourceManager.getApiVersion(apiVersion.getId()));
            } catch (ApiException e) {}
        });
    }

    private void delete(ApiVersion apiVersion) throws ApiException {
        apiVersion.getImplementationsVersions().forEach(implementationVersion -> {
            try {
                implementationVersionsClientResourceManager.deleteImplementationVersion(implementationVersion.getId());
            } catch (ApiException e) {
                e.printStackTrace();
            }
        });
        apiVersion.getLibrariesApiVersions().forEach(libraryApiVersion -> {
            try {
                libraryApiVersionsClientResourceManager.deleteLibraryApiVersion(libraryApiVersion.getId());
            } catch (ApiException e) {
                e.printStackTrace();
            }
        });
        apiVersionsClientResourceManager.deleteApiVersion(apiVersion.getId());
    }

    @Then("^I have a new apiversion '(.+)' for component '(.+)' with status '(.+)'$")
    public void checkNewApiVersion(String version, String componentName, String status) {
        check(apiVersionAdded, version, createdComponents.get(componentName), status);
    }

    @Then("^I have a apiversion '(.+)' for component '(.+)' with status '(.+)'$")
    public void checkExistingApiVersion(String version, String componentName, String status) {
        check(createdApiVersions.get(version), version, createdComponents.get(componentName), status);
    }

    private void check(ApiVersion apiVersion, String version, Component component, String status){
        Assert.assertNotNull(apiVersion);
        Assert.assertTrue(apiVersion.getVersion().contains(version));
        Assert.assertEquals(status, apiVersion.getStatus().toString());
        Assert.assertEquals(component.getId(), apiVersion.getComponent().getId());
    }

    private File getFileCustomized(File file, String version, String artifactName) throws Exception {
        File newFile = new File(System.getProperty("java.io.tmpdir")+"/"+file.getName()+"-"+version+".yaml");
        String content = IOUtils.toString(new FileInputStream(file), Encoding.DEFAULT_ENCODING);
        content = content.replaceAll("##version##", version);
        content = content.replaceAll("##artifactName##", artifactName);
        IOUtils.write(content, new FileOutputStream(newFile), Encoding.DEFAULT_ENCODING);
        return newFile;
    }

    @Then("^Wait until the new apiversion is '(.+)', max timeout (\\d+) sec$")
    public void checkNewUntilHasStatusOrFailed(String status, int timeout) throws ApiException, InterruptedException {
        checkUntilHasStatusOrFailed(Resource.StatusEnum.PENDING, status, timeout);
    }
    @Then("^Wait until the updating apiversion is '(.+)', max timeout (\\d+) sec$")
    public void checkUpdatingUntilHasStatusOrFailed(String status, int timeout) throws ApiException, InterruptedException {
        checkUntilHasStatusOrFailed(Resource.StatusEnum.UPDATING, status, timeout);
    }

    public void checkUntilHasStatusOrFailed(Resource.StatusEnum initialStatus, String status, int timeout) throws ApiException, InterruptedException {
        Assert.assertNotNull(apiVersionAdded);
        ApiVersion apiVersion;
        long startTime = System.currentTimeMillis();
        do {
            apiVersion = apiVersionsClientResourceManager.getApiVersion(apiVersionAdded.getId());
            Assert.assertNotNull(apiVersion);
            Thread.sleep(1000);
        } while(apiVersion.getStatus().toString().equalsIgnoreCase(initialStatus.toString()) &&
                System.currentTimeMillis() - startTime < timeout * 1000);
        Assert.assertEquals(status, apiVersion.getStatus().toString());

        Component componentWithDetails = componentsClientAppManager.getComponentById(apiVersionAdded.getComponent().getId());
        Optional<ApiVersion> apiVersionFound = componentWithDetails.getVersions().stream().filter(i -> i.getVersion().equals(apiVersionAdded.getVersion())).findFirst();
        Assert.assertTrue(apiVersionFound.isPresent());
    }

    @Then("ApiFile '(.*)' for component '(.*)' at version '(.*)' is available")
    public void checkApiFileIsAvailable(String filename, String componentName, String version) throws Exception {
        Component component = createdComponents.get(componentName);
        Component componentWithDetails = componentsClientAppManager.getComponentById(component.getId());
        Optional<ApiVersion> apiVersionFound = componentWithDetails.getVersions().stream().filter(apiVersion -> apiVersion.getVersion().equals(version)).findFirst();
        Assert.assertTrue(apiVersionFound.isPresent());

        File fileExpected = getFileCustomized(new File("./src/main/resources/org/kathra/e2e/" + filename), version, component.getName());

        File file = apiVersionsClientAppManager.getApiFile(apiVersionFound.get().getId());
        Assert.assertTrue(file.exists());

        String contentExpected = IOUtils.toString(new FileInputStream(fileExpected), Encoding.DEFAULT_ENCODING);
        String contentReturned = IOUtils.toString(new FileInputStream(file), Encoding.DEFAULT_ENCODING);
        Assert.assertEquals(contentExpected, contentReturned);
    }


    @When("^All apiversion's libraries '(.+)' are '(.+)'$")
    public void checkLibApiVersion(String version, String status) throws ApiException {
        ApiVersion apiVersion = apiVersionsClientResourceManager.getApiVersion(createdApiVersions.get(version).getId());
        List<LibraryApiVersion> libs = apiVersion.getLibrariesApiVersions();
        for(LibraryApiVersion lib:libs) {
            LibraryApiVersion libWithDetails = libraryApiVersionsClientResourceManager.getLibraryApiVersion(lib.getId());
            Assert.assertEquals(status, libWithDetails.getStatus().toString());
            Assert.assertEquals(status, libWithDetails.getApiRepositoryStatus().toString());
            Assert.assertEquals(status, libWithDetails.getPipelineStatus().toString());
        }
    }
}