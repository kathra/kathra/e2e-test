/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */
package org.kathra.e2e.utils;

import org.kathra.appmanager.client.ApiVersionsClient;
import org.kathra.appmanager.client.ComponentsClient;
import org.kathra.appmanager.client.GroupsClient;
import org.kathra.appmanager.client.ImplementationsClient;
import org.kathra.core.model.ApiVersion;
import org.kathra.core.model.Component;
import org.kathra.core.model.Implementation;
import org.kathra.utils.KathraSessionManager;

import java.util.HashMap;
import java.util.Map;

public class KathraApiClients {

    public static Component componentAdded;
    public static Map<String,Component> createdComponents = new HashMap<>();

    public static ApiVersion apiVersionAdded;
    public static Map<String,ApiVersion> createdApiVersions = new HashMap<>();

    public static Map<String, Implementation> createdImplementations = new HashMap<>();

    public static GroupsClient groupsClientAppManager;
    public static ComponentsClient componentsClientAppManager;
    public static ApiVersionsClient apiVersionsClientAppManager;
    public static ImplementationsClient implementationsClientAppManager;

    public static org.kathra.resourcemanager.client.ComponentsClient componentsClientResourceManager;
    public static org.kathra.resourcemanager.client.ApiVersionsClient apiVersionsClientResourceManager;
    public static org.kathra.resourcemanager.client.LibrariesClient librairiesClientResourceManager;
    public static org.kathra.resourcemanager.client.LibraryApiVersionsClient libraryApiVersionsClientResourceManager;
    public static org.kathra.resourcemanager.client.SourceRepositoriesClient repositoryClientResourceManager;
    public static org.kathra.resourcemanager.client.PipelinesClient pipelineClientResourceManager;
    public static org.kathra.resourcemanager.client.ImplementationsClient implementationsClientResourceManager;
    public static org.kathra.resourcemanager.client.ImplementationVersionsClient implementationVersionsClientResourceManager;

    public static void init() {
        init(new Config(), new SessionManagerImpl());
    }

    public static void init(String userLogin, String userPwd) {
        init(new Config(userLogin, userPwd), new SessionManagerImpl());
    }

    public static void init(Config config, KathraSessionManager sessionManager) {

        groupsClientAppManager = new GroupsClient(config.getKathraAppManagerUrl(), sessionManager);
        componentsClientAppManager = new ComponentsClient(config.getKathraAppManagerUrl(), sessionManager);
        apiVersionsClientAppManager = new ApiVersionsClient(config.getKathraAppManagerUrl(), sessionManager);
        implementationsClientAppManager = new ImplementationsClient(config.getKathraAppManagerUrl(), sessionManager);

        componentsClientResourceManager = new org.kathra.resourcemanager.client.ComponentsClient(config.getKathraResourceManagerUrl(), sessionManager);
        apiVersionsClientResourceManager = new org.kathra.resourcemanager.client.ApiVersionsClient(config.getKathraResourceManagerUrl(), sessionManager);
        implementationsClientResourceManager = new org.kathra.resourcemanager.client.ImplementationsClient(config.getKathraResourceManagerUrl(), sessionManager);
        implementationVersionsClientResourceManager = new org.kathra.resourcemanager.client.ImplementationVersionsClient(config.getKathraResourceManagerUrl(), sessionManager);

        repositoryClientResourceManager = new org.kathra.resourcemanager.client.SourceRepositoriesClient(config.getKathraResourceManagerUrl(), sessionManager);
        pipelineClientResourceManager = new org.kathra.resourcemanager.client.PipelinesClient(config.getKathraResourceManagerUrl(), sessionManager);
        librairiesClientResourceManager = new org.kathra.resourcemanager.client.LibrariesClient(config.getKathraResourceManagerUrl(), sessionManager);
        libraryApiVersionsClientResourceManager = new org.kathra.resourcemanager.client.LibraryApiVersionsClient(config.getKathraResourceManagerUrl(), sessionManager);

    }
}
