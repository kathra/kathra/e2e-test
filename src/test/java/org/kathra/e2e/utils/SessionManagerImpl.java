/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */
package org.kathra.e2e.utils;

import org.kathra.utils.Session;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.security.KeycloakUtils;

public class SessionManagerImpl implements KathraSessionManager {

    Config config = new Config();
    Session currentSession = null;

    public Session getCurrentSession() {
        if (currentSession == null) {
            currentSession = new Session().accessToken(KeycloakUtils.login(config.getUserLogin(), config.getUserPassword())).authenticated(true);
        }
        return currentSession;
    }
}
