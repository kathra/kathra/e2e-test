/* 
 * Copyright 2019 The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *
 *    IRT SystemX (https://www.kathra.org/)    
 *
 */
package org.kathra.e2e.implementation;


import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.util.Encoding;
import org.kathra.appmanager.model.ImplementationParameters;
import org.kathra.core.model.*;
import org.kathra.e2e.utils.KathraApiClients;
import org.kathra.utils.ApiException;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Optional;

public class ImplementationSteps extends KathraApiClients {


    @When("^I add '(.+)' implementation '(.+)' from component '(.+)'@'(.+)'$")
    public void create(String lang, String name, String componentName, String componentVersion) throws Exception {
        Component component = createdComponents.get(componentName);
        Component componentWithApiVersions = componentsClientAppManager.getComponentById(component.getId());
        ApiVersion apiVersion = componentWithApiVersions.getVersions().stream().filter(v -> v.getVersion().equals(componentVersion)).findFirst().get();
        String nameNoExisting = getNoExistingName(name);
        ImplementationParameters implParams = new ImplementationParameters().apiVersion(apiVersion).name(nameNoExisting).language(lang);
        Implementation implementationCreated = implementationsClientAppManager.createImplementation(implParams);
        createdImplementations.put(name, implementationCreated);
    }

    @Then("^I have a new '(.+)' implementation '(.+)' for component '(.+)'@'(.+)' with status '(.+)'$")
    public void check(String lang, String name, String componentName, String componentVersion, String status) throws ApiException {
        Implementation implementation = createdImplementations.get(name);
        Assert.assertEquals(lang, implementation.getLanguage().toString());
        Assert.assertEquals(createdImplementations.get(name).getName(), implementation.getName());
        Assert.assertEquals(status, implementation.getStatus().toString());
        Assert.assertEquals(implementation.getComponent().getId(), createdComponents.get(componentName).getId());
    }

    private String getNoExistingName(String prefixName) throws ApiException {
        String nameGenerated = prefixName;
        List<Implementation> implementations = implementationsClientResourceManager.getImplementations();
        if (!nameAlreadyExist(implementations, nameGenerated)) {
            return nameGenerated;
        }
        int i=0;
        do {
            nameGenerated = prefixName+"-"+i;
            i++;
        } while(nameAlreadyExist(implementations, nameGenerated));
        return nameGenerated;
    }

    private boolean nameAlreadyExist(List<Implementation> implementations, String name){
        return implementations.stream().anyMatch(implementation -> implementation.getName().equalsIgnoreCase(name));
    }

    @When("Remove all implementation for '(.+)' component exists")
    public void removeImplementationsFromComponentName(String componentName) throws ApiException {
        Optional<Component> componentFound = componentsClientResourceManager.getComponents().stream().filter(component -> component.getName().equals(componentName)).findFirst();
        componentFound.get().getImplementations().stream().forEach(implementation -> {
            try {
                delete(implementationsClientResourceManager.getImplementation(implementation.getId()));
            } catch (ApiException e) {}
        });
    }

    private void delete(Implementation implementation) throws ApiException {
        implementation.getVersions().forEach(implementationVersion -> {
            try {
                delete(implementationVersionsClientResourceManager.getImplementationVersion(implementationVersion.getId()));
            } catch (ApiException e) {

            }
        });
        pipelineClientResourceManager.deletePipeline(implementation.getPipeline().getId());
        repositoryClientResourceManager.deleteSourceRepository(implementation.getSourceRepository().getId());
        implementationsClientResourceManager.deleteImplementation(implementation.getId());
    }

    private void delete(ImplementationVersion implementationVersion) throws ApiException {
        implementationVersionsClientResourceManager.deleteImplementationVersion(implementationVersion.getId());
    }


    @Then("^Wait until the new '(.+)' implementation '(.+)' is '(.+)', max timeout (\\d+) sec$")
    public void checkUntilHasStatusOrFailed(String lang, String name, String status, int timeout) throws ApiException, InterruptedException {
        Assert.assertNotNull(createdImplementations.get(name));
        Implementation implementation;
        long startTime = System.currentTimeMillis();
        do {
            implementation = implementationsClientAppManager.getImplementationById(createdImplementations.get(name).getId());
            Assert.assertNotNull(implementation);
            Thread.sleep(1000);
        }
        while (implementation.getStatus().toString().equalsIgnoreCase(Resource.StatusEnum.PENDING.toString()) && System.currentTimeMillis() - startTime < timeout * 1000);
        Assert.assertEquals(status, implementation.getStatus().toString());

        Assert.assertEquals(lang, implementation.getLanguage().getValue());
        Assert.assertEquals(1, implementation.getVersions().size());
        Assert.assertNotNull(implementation.getSourceRepository().getId());
        Assert.assertNotNull(implementation.getPipeline().getId());

        Component componentWithDetails = componentsClientAppManager.getComponentById(implementation.getComponent().getId());
        Assert.assertNotNull(componentWithDetails);
    }

    @Then("^The new implementation '(.+)' has a new '(.*)' at version '(.*)' implementing the component '(.+)'@'(.+)'$")
    public void checkImplVersion(String name, String status, String version, String componentName, String componentVersion) throws ApiException, InterruptedException {

        Implementation implementation = implementationsClientAppManager.getImplementationById(createdImplementations.get(name).getId());
        ImplementationVersion implementationVersion = implementation.getVersions().stream().map(v -> {
            try {
                return implementationVersionsClientResourceManager.getImplementationVersion(v.getId());
            } catch (ApiException e) {
                e.printStackTrace();
                return null;
            }
        }).filter(v -> v.getVersion().equals(version)).findFirst().get();
        ApiVersion apiVersion = componentsClientAppManager.getComponentById(createdComponents.get(componentName).getId()).getVersions().stream().filter(v -> v.getVersion().equals(componentVersion)).findFirst().get();

        Assert.assertEquals(status, implementationVersion.getStatus().toString());
        Assert.assertEquals(apiVersion.getId(), implementationVersion.getApiVersion().getId());

    }
}